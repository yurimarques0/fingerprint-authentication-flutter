import 'package:local_auth/auth_strings.dart';
import 'package:local_auth/local_auth.dart';

class Authentication {
  static Future<bool> authenticateWithBiometrics() async {
    final LocalAuthentication localAuthentication = LocalAuthentication();
    bool isBiometricSupported = await localAuthentication.canCheckBiometrics;
    bool canCheckBiometrics = await localAuthentication.canCheckBiometrics;

    bool isAuthenticated = false;

    if (isBiometricSupported && canCheckBiometrics) {
      isAuthenticated = await localAuthentication.authenticateWithBiometrics(
          localizedReason: 'Please complete the biometrics to proceed.',
          stickyAuth: false,
          useErrorDialogs: true,
          androidAuthStrings:
              AndroidAuthMessages(signInTitle: "Login to HomePage"));
    }

    return isAuthenticated;
  }
}
